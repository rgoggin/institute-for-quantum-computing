core = 7.x
api = 2

; uw_feed_iqc_publications
projects[uw_feed_iqc_publications][type] = "module"
projects[uw_feed_iqc_publications][download][type] = "git"
projects[uw_feed_iqc_publications][download][url] = "https://git.uwaterloo.ca/wcms/uw_feed_iqc_publications.git"
projects[uw_feed_iqc_publications][download][tag] = "7.x-1.0"
projects[uw_feed_iqc_publications][subdir] = ""

; uw_feed_iqc_visits
projects[uw_feed_iqc_visits][type] = "module"
projects[uw_feed_iqc_visits][download][type] = "git"
projects[uw_feed_iqc_visits][download][url] = "https://git.uwaterloo.ca/wcms/uw_feed_iqc_visits.git"
projects[uw_feed_iqc_visits][download][tag] = "7.x-1.1"
projects[uw_feed_iqc_visits][subdir] = ""

; uw_iqc_people
projects[uw_iqc_people][type] = "module"
projects[uw_iqc_people][download][type] = "git"
projects[uw_iqc_people][download][url] = "https://git.uwaterloo.ca/iqc/uw_iqc_people.git"
projects[uw_iqc_people][download][tag] = "7.x-1.0"
projects[uw_iqc_people][subdir] = ""
